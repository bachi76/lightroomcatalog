# Lightroom Catalog Access java library #

This library provides easy access to the Lightroom catalog data and preview store. It was developed against LR 5.2 but should work also with later versions.

The api provides easy ORM-style access to the LR catalog.

It was developed to be able to run a web application on a remote server to the full LR catalog and images through the preview catalog. For this, I backup my catalog and preview store to that remote server automatically using rsync.

## Demo application ##
There is a sample (play-based) web application that lets you browse the folders and collections, and will extract and show the previews. It should give a good overview of the api usage.

https://bitbucket.org/bachi76/lightroom-web-frontend/

## Usage examples: ##

*Get a list of folders:*
(To get collections instead, you'd use  ```catalog.collections()```)

	:::java
	catalog = new LrCatalog(lrcatPath, lrdataFolderPath, imageFolderPath);

	List<LrFolder> folders = catalog.folders().queryBuilder()
		.orderBy("pathFromRoot", true)
		.query();

*Get previews for a selected folder:*

	:::java
	catalog = new LrCatalog(lrcatPath, lrdataFolderPath, imageFolderPath);	     
	long id = <some folder id>;
	int DEFAULT_PREV = 4; // 1-6 usually - marks preview size, depends on LR catalog setting

	List<LrImageAccessor> images = new ArrayList<>();

	try {
		LrFolder folder = catalog.folders().queryForId(id);

		for (LrImage lrimg : catalog.images().in(folder)) {
		    LrImageAccessor image = catalog.image(lrimg);

		    if (image.previews().size() == 0){
		        Logger.warn("No preview found for image id: " + image.id());
		        continue;
		    }
		    image.setPreviewLevel(DEFAULT_PREV);
		    images.add(image);
		}

		return ok(views.html.folder.render(folder.getPathFromRoot(), images));


	} catch (SQLException e) {
		e.printStackTrace();          
	}
        

*Accessing a preview image*

	:::java
	LrImageAccessor image = catalog.image(imageId);
	Optional<File> prev = image.previewFile(previewIndex);

	// Preview not found yet, try to extract
	if (!prev.isPresent()) {
		image.extract();
		prev = image.previewFile(previewIndex);
	}

## Notes ##

* The Lr prefixed model classes represent 1:1 the lrcatalog db tables (a subset thereof). You can use the [ORMLite DAOs](http://ormlite.com/javadoc/ormlite-core/doc-files/ormlite_3.html#QueryBuilder-Basics) to query them. For easy access to an image you can use the LrImageAccessor which consolidates its different entities.

* Writing back to the db was not tested, but should be possible. E.g for setting rating stars from the web application (first you need to add the corresponding Lr model classes).


## Dependencies ##

Are managed by maven. ORMLite (http://ormlite.com/) and the Sqlite jdbc driver are required.

## Contributiors ##

Feel free - there is lots of room for enhancements and improvements. PRs or regular contributors most welcome.

### Who do I talk to? ###

Bachi - bachi.insign at gmail dot com