package ch.insign.lightroom.dao;

import ch.insign.lightroom.model.LrCollection;
import ch.insign.lightroom.model.LrFolder;
import ch.insign.lightroom.model.LrImage;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Access images.
 */
public class LrImageDao extends BaseDaoImpl<LrImage, Long> {

	public LrImageDao(ConnectionSource connectionSource, Class<LrImage> dataClass) throws SQLException {
		super(connectionSource, dataClass);
	}

	/**
	 * Get all images in the given folder.
	 * @throws SQLException
	 */
	public List<LrImage> in(LrFolder folder) throws SQLException {

		String query = "SELECT ai.id_local\n" +
			"FROM Adobe_images ai \n" +
			"LEFT JOIN AgLibraryFolderStackImage fsi ON ai.id_local = fsi.image \n" +
			"LEFT JOIN AgLibraryFile fi on ai.rootFile = fi.id_local \n" +
			"LEFT JOIN AgLibraryFolder fo on fi.folder = fo.id_local \n" +
			"WHERE fo.id_local = ?";

		// Convert to id list
		List<Long> ids = this.queryRaw(query, String.valueOf(folder.getId_local())).getResults()
			.stream()
			.map(result -> Long.valueOf(result[0]))
			.collect(Collectors.toList());

		return this.query(
			this.queryBuilder()
				.where().in("id_local", ids).prepare()
		);
	}

	/**
	 * Get all images of the given collection (sorted).
	 * @throws SQLException
	 */
	public List<LrImage> in(LrCollection collection) throws SQLException {

		String query = "SELECT ai.id_local\n" +
			"FROM Adobe_images ai \n" +
			"JOIN AgLibraryCollectionImage ci ON ai.id_local = ci.image \n" +
			"JOIN AgLibraryCollection c on ci.collection = c.id_local \n" +
			"WHERE c.id_local = ? \n" +
			"ORDER BY ci.positionInCollection ASC";

		// Convert to id list
		List<Long> ids = this.queryRaw(query, String.valueOf(collection.getId_local())).getResults()
			.stream()
			.map(result -> Long.valueOf(result[0]))
			.collect(Collectors.toList());

		return this.query(
			this.queryBuilder()
				.where().in("id_local", ids).prepare()
		);

	}

	/**
	 * Get an image by its uuid / id_global
	 * @throws SQLException
	 */
	public Optional<LrImage> uuid(String uuid) throws SQLException {
		List<LrImage> results = queryForEq("id_global", uuid);
		if (results.size() == 0) return Optional.empty();

		return Optional.of(results.get(0));
	}
}
