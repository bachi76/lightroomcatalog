package ch.insign.lightroom.dao;

import ch.insign.lightroom.model.LrFile;
import ch.insign.lightroom.model.LrFolder;
import ch.insign.lightroom.model.LrPreview;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * Access preview.db's PyramidLevel table which contains the preview entries.
 */
public class LrPreviewDao extends BaseDaoImpl<LrPreview, Long> {

	public LrPreviewDao(ConnectionSource connectionSource, Class<LrPreview> dataClass) throws SQLException {
		super(connectionSource, dataClass);
	}

	/**
	 * Get all previews of a file by its uuid
	 * Ordered by level asc
	 *
	 * @throws java.sql.SQLException
	 */
	public List<LrPreview> uuid(String uuid) throws SQLException {

		return query(
			queryBuilder().orderBy("level", true).where().eq("uuid", uuid).prepare()
		);
	}

	/**
	 * Get all previews of a file.
	 * Ordered by level asc.
	 *
	 * @throws SQLException
	 */
	public List<LrPreview> forFile(LrFile file) throws SQLException {
		return uuid(file.getId_global());
	}
}
