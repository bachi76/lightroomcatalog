package ch.insign.lightroom.dao;

import ch.insign.lightroom.LrCatalog;
import ch.insign.lightroom.model.LrFile;
import ch.insign.lightroom.model.LrFolder;
import ch.insign.lightroom.model.LrImage;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Access the files.
 */
public class LrFileDao extends BaseDaoImpl<LrFile, Long> {

	private LrCatalog catalog;

	public LrFileDao(ConnectionSource connectionSource, Class<LrFile> dataClass) throws SQLException {
		super(connectionSource, dataClass);
	}

	public void setCatalog(LrCatalog catalog) {
		this.catalog = catalog;
	}

	/**
	 * Get all files in a folder.
	 * @throws SQLException
	 */
	public List<LrFile> in(LrFolder folder) throws SQLException {
		return queryBuilder()
			.where().eq("folder", folder.getId_local())
			.query();
	}

	/**
	 * Get a file by its uuid / id_global
	 * @throws java.sql.SQLException
	 */
	public Optional<LrFile> uuid(String uuid) throws SQLException {
		List<LrFile> results = queryForEq("id_global", uuid);
		if (results.size() == 0) return Optional.empty();

		return Optional.of(results.get(0));
	}

	/**
	 * Get the file that belongs to the given image id.
	 * @throws SQLException
	 */
	public Optional<LrFile> of(Long imageId) throws SQLException {
		LrImage image = catalog.images().queryForId(imageId);
		LrFile file = catalog.files().queryForId(image.getRootFile());

		return Optional.ofNullable(file);
	}

	/**
	 * Get the file that belongs to the given image.
	 * @throws SQLException
	 */
	public Optional<LrFile> of(LrImage image) throws SQLException {
		return of(image.getId_local());
	}
}
