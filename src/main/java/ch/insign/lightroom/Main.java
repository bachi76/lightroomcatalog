package ch.insign.lightroom;

import ch.insign.lightroom.model.*;

import java.sql.SQLException;

/**
 * Created by bachi on 05.01.15.
 */
public class Main {

	public static void main(String[] args) {

		extractEverything();
		return;

		/*try {



			String lrdataFolderPath = "/home/bachi/Desktop/Lightroom_5_Catalog_Previews.lrdata";
			String imageFolderPath = "/home/bachi/Desktop/LR-Output";
			String lrcatPath = "/home/bachi/Desktop/Lightroom_5_Catalog.lrcat";

			LrCatalog catalog = new LrCatalog("jdbc:sqlite:" + lrcatPath, lrdataFolderPath, imageFolderPath);

			System.out.println(catalog.collections().countOf());


			for (LrCollection lrCollection : catalog.collections()) {
				System.out.println(lrCollection.getName());
			}

			LrFolder folder = catalog.folders().queryForAll().get(7);
			System.out.println("\nImages of " + folder.getPathFromRoot());

			for (LrImage image : catalog.images().in(folder)) {
				System.out.println(image.getCaptureTime());
			}

			// Extraction test
			LrImage extrImage = catalog.images().uuid("8924E7C6-03F7-4BA3-BD48-324538DC96EF").get();
			LrFile extrFile = catalog.files().queryForId(extrImage.getRootFile());
			LrFolder extrFolder = catalog.folders().queryForId(extrFile.getFolder());


			//catalog.previewStore().extract(extrFile, extrFolder.getPathFromRoot());

			System.out.println(
				"Preview size: " + catalog.previewStore().getPreview(extrFile, 3).get().length()
			);


		} catch (SQLException e) {
			e.printStackTrace();
		}*/

	}

	private static void extractEverything() {

		String lrdataFolderPath = "/home/bachi/Desktop/Lightroom_5_Catalog_Previews.lrdata";
		String imageFolderPath = "/home/bachi/Desktop/LR-Output";
		String lrcatPath = "/home/bachi/Desktop/Lightroom_5_Catalog.lrcat";

		try {
			LrCatalog catalog = new LrCatalog(lrcatPath, lrdataFolderPath, imageFolderPath);

			for (LrImageCacheEntry cacheEntry : catalog.previewStore().cacheEntries()) {
				LrFile file = catalog.files().of(cacheEntry.getImageId()).get();
				catalog.previewStore().getPreview(file, 99);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}


	}
}
