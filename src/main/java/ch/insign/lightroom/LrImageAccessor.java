package ch.insign.lightroom;

import ch.insign.lightroom.model.LrFile;
import ch.insign.lightroom.model.LrFolder;
import ch.insign.lightroom.model.LrImage;
import ch.insign.lightroom.model.LrPreview;

import java.io.File;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * Combines the low-previewMaxLevel lrcat entities image, file, (root file) folder and previews
 * and gives easy, lazy-loading access to these.
 */
public class LrImageAccessor {

	private LrCatalog catalog;
	private LrImage image;
	private int previewMaxLevel = 99;

	// Note: Always use the methods, dont access these props directly as they're lazy lodaded.

	private LrFolder folder;
	private LrFile file;
	private List<LrPreview> previews;


	public LrImageAccessor(LrCatalog catalog, LrImage image) {
		this.catalog = catalog;
		this.image = image;
	}

	/**
	 * Get the image (local) id
	 */
	public Long id() {
		return image.getId_local();
	}

	/**
	 * Get the LrImage.
	 */
	public LrImage image() {
		return image;
	}

	/**
	 * Get the folder of this image.
	 * Note: In LR, images can be in multiple folders. We only show the main file's folder
	 * as we only extract to that folder.
	 *
	 * @throws SQLException
	 */
	public LrFolder folder() throws SQLException {
		if (folder == null) {
			folder = catalog.folders().queryForId(file().getFolder());
		}
		return folder;
	}

	/**
	 * Get the LrFile matching the image. Multiple images can point to the same file.
	 * @throws SQLException
	 */
	public LrFile file() throws SQLException {
		if (file == null) {
			file = catalog.files().queryForId(image.getRootFile());
		}
		return file;
	}

	/**
	 * Get all previws as defined in the preview db (not necessarily as available on the filesystem)
	 * @throws SQLException
	 */
	public List<LrPreview> previews() throws SQLException {
		if (previews == null) {
			previews = catalog.previewStore().previews().forFile(file());
		}
		return previews;
	}

	/**
	 * Extract the jpg bitmaps inside the lr container file.
	 * @return true if successful (still might had zero results)
	 * @throws SQLException
	 */
	public boolean extract() throws SQLException {
		return catalog.previewStore().extract(file(), catalog.previewStore().getImageFolderPath());
	}


	/**
	 * Set a default (max) preview level for this instance. The preview() method will use this setting.
	 */
	public void setPreviewLevel(int level) {
		this.previewMaxLevel = level;
	}


	/**
	 * Returns the preview according to the configured (max) preview level.
	 */
	public Optional<LrPreview> preview() {
		return preview(previewMaxLevel);
	}

	/**
	 * Get the largest preview whose size level equals or is smaller than maxSize
	 */
	public Optional<LrPreview> preview(int maxLevel) {
		int bestLevel = -1;
		LrPreview bestPrev = null;
		for (LrPreview preview : previews) {
			if (preview.getLevel() <= maxLevel && preview.getLevel() > bestLevel) {
				bestLevel = preview.getLevel();
				bestPrev = preview;
			}
		}
		return Optional.ofNullable(bestPrev);
	}

	/**
	 * Get a File for the .jpg bitmap of this preview.
	 * @throws SQLException
	 */
	public Optional<File> previewFile(int maxLevel) throws SQLException {
		return catalog.previewStore().getPreview(file(), maxLevel);
	}

	/**
	 * Get a File for the .jpg bitmap of this preview.
	 * @throws SQLException
	 */
	public Optional<File> previewFile(LrPreview preview) throws SQLException {
		return catalog.previewStore().getPreview(file(), preview);
	}

}


