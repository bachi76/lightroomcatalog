package ch.insign.lightroom;

import ch.insign.lightroom.dao.LrFileDao;
import ch.insign.lightroom.dao.LrImageDao;
import ch.insign.lightroom.model.*;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.LruObjectCache;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

/**
 * Lightroom catalog.
 */
public class LrCatalog {

	static final int LRU_CACHE_SIZE = 100;
	private final ConnectionSource connectionSource;
	private final LrPreviewStore previewStore;

	private Dao<LrCollection, Long> collectionDao;
	private Dao<LrFolder, Long> folderDao;
	private LrImageDao imageDao;
	private LrFileDao fileDao;
	private Dao<LrCollectionImage, Long> collectionImageDao;
	private Dao<LrFolderImage, Long> folderImageDao;

	/**
	 * Initialize the LR catalog.
	 *
	 * @param lrcatPath The full path to the .lrcat catalog db file.
	 * @param lrdataFolderPath The full path to the .lrdata preview folder.
	 * @param imageFolderPath The full path to the desired image output root folder.
	 * @throws SQLException
	 */
	public LrCatalog(String lrcatPath, String lrdataFolderPath, String imageFolderPath) throws SQLException {

		String databaseUrl = "jdbc:sqlite:" + lrcatPath;

		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		// create a connection source to our database
		connectionSource = new JdbcConnectionSource(databaseUrl);
		System.out.println("Connection status: " + connectionSource.getReadOnlyConnection().isTableExists("adobe_images"));

		previewStore = new LrPreviewStore(this, lrdataFolderPath, imageFolderPath);

	}

	public ConnectionSource connectionSource() {
		return connectionSource;
	}

	public LrPreviewStore previewStore() {
		return previewStore;
	}

	public Dao<LrCollection, Long> collections() throws SQLException {
		if (collectionDao ==  null) {
			collectionDao = DaoManager.createDao(connectionSource, LrCollection.class);
			collectionDao.setObjectCache(new LruObjectCache(LRU_CACHE_SIZE));
		}
		return collectionDao;
	}

	public Dao<LrCollectionImage, Long> collectionImages() throws SQLException {
		if (collectionImageDao ==  null) {
			collectionImageDao = DaoManager.createDao(connectionSource, LrCollectionImage.class);
			collectionImageDao.setObjectCache(new LruObjectCache(LRU_CACHE_SIZE));
		}
		return collectionImageDao;
	}

	public Dao<LrFolder, Long> folders() throws SQLException {
		if (folderDao ==  null) {
			folderDao = DaoManager.createDao(connectionSource, LrFolder.class);
			folderDao.setObjectCache(new LruObjectCache(LRU_CACHE_SIZE));
		}
		return folderDao;
	}

	public Dao<LrFolderImage, Long> folderImages() throws SQLException {
		if (folderImageDao ==  null) {
			folderImageDao = DaoManager.createDao(connectionSource, LrFolderImage.class);
			folderImageDao.setObjectCache(new LruObjectCache(LRU_CACHE_SIZE));
		}
		return folderImageDao;
	}

	public LrImageDao images() throws SQLException {
		if (imageDao ==  null) {
			imageDao = DaoManager.createDao(connectionSource, LrImage.class);
			imageDao.setObjectCache(new LruObjectCache(LRU_CACHE_SIZE));
		}
		return (LrImageDao) imageDao;
	}

	public LrFileDao files() throws SQLException {
		if (fileDao ==  null) {
			fileDao = DaoManager.createDao(connectionSource, LrFile.class);
			fileDao.setObjectCache(new LruObjectCache(LRU_CACHE_SIZE));
			fileDao.setCatalog(this);
		}
		return (LrFileDao) fileDao;
	}

	/**
	 * Returns an image accessor helper instance which lets you easly
	 * access related entities.
	 */
	public LrImageAccessor image(LrImage lrImage) {
		return new LrImageAccessor(this, lrImage);
	}

	/**
	 * Returns an image accessor helper instance which lets you easly
	 * access related entities.
	 */
	public LrImageAccessor image(Long lrImageId) throws SQLException {
		return new LrImageAccessor(this, images().queryForId(lrImageId));
	}

}
