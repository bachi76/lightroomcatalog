package ch.insign.lightroom.model;

import ch.insign.lightroom.dao.LrPreviewDao;
import com.j256.ormlite.table.DatabaseTable;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * Access to the preview.db's PyramidLevel table.
 */
@Entity(name = "PyramidLevel")
@DatabaseTable(daoClass = LrPreviewDao.class)
public class LrPreview {

	@Column private String uuid;
	@Column private String digest;
	@Column private Integer level;
	@Column private Integer longDimension;
	@Column private Integer width;
	@Column private Integer height;

	// # auto-generated

	public String getDigest() {
		return digest;
	}

	public void setDigest(String digest) {
		this.digest = digest;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getLongDimension() {
		return longDimension;
	}

	public void setLongDimension(Integer longDimension) {
		this.longDimension = longDimension;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}
}
