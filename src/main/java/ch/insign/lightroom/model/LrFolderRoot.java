package ch.insign.lightroom.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Access lightroom folders.
 */
@Entity(name = "AgLibraryFolder")
public class LrFolderRoot {

	@Id private Long id_local;
	@Column	private String id_global;
	@Column private String absolutePath;
	@Column private String name;
	@Column private String relativePathFromCatalog;

	// # auto-generated

	public String getAbsolutePath() {
		return absolutePath;
	}

	public void setAbsolutePath(String absolutePath) {
		this.absolutePath = absolutePath;
	}

	public String getId_global() {
		return id_global;
	}

	public void setId_global(String id_global) {
		this.id_global = id_global;
	}

	public Long getId_local() {
		return id_local;
	}

	public void setId_local(Long id_local) {
		this.id_local = id_local;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRelativePathFromCatalog() {
		return relativePathFromCatalog;
	}

	public void setRelativePathFromCatalog(String relativePathFromCatalog) {
		this.relativePathFromCatalog = relativePathFromCatalog;
	}
}
