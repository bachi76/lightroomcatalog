package ch.insign.lightroom.model;

import ch.insign.lightroom.dao.LrFileDao;
import ch.insign.lightroom.dao.LrImageDao;
import com.j256.ormlite.table.DatabaseTable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Optional;

/**
 * Access lightroom images.
 *
 */
@Entity(name = "AgLibraryFile")
@DatabaseTable(daoClass = LrFileDao.class)
public class LrFile {
	@Id	private Long id_local;
	@Column private String id_global;
	@Column private String baseName;
	@Column private String extension;
	@Column private String errorMessage;
	@Column private Integer errorTime;
	@Column private Long folder;
	@Column private String originalFilename;

	// # auto-generated

	public String getBaseName() {
		return baseName;
	}

	public void setBaseName(String baseName) {
		this.baseName = baseName;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Integer getErrorTime() {
		return errorTime;
	}

	public void setErrorTime(Integer errorTime) {
		this.errorTime = errorTime;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public Long getFolder() {
		return folder;
	}

	public void setFolder(Long folder) {
		this.folder = folder;
	}

	public String getId_global() {
		return id_global;
	}

	public void setId_global(String id_global) {
		this.id_global = id_global;
	}

	public Long getId_local() {
		return id_local;
	}

	public void setId_local(Long id_local) {
		this.id_local = id_local;
	}

	public String getOriginalFilename() {
		return originalFilename;
	}

	public void setOriginalFilename(String originalFilename) {
		this.originalFilename = originalFilename;
	}
}
