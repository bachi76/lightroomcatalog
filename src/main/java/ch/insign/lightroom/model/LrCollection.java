package ch.insign.lightroom.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Access lightroom collections.
 */
@Entity(name = "aglibrarycollection")
public class LrCollection {

	@Id private Long id_local;
	@Column private String name;
	@Column private Long parent;


	// # auto-generated

	public Long getId_local() {
		return id_local;
	}

	public void setId_local(Long id_local) {
		this.id_local = id_local;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getParent() {
		return parent;
	}

	public void setParent(Long parent) {
		this.parent = parent;
	}
}
