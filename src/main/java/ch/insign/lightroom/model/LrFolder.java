package ch.insign.lightroom.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Access lightroom folders.
 */
@Entity(name = "AgLibraryFolder")
public class LrFolder {

	@Id private Long id_local;
	@Column	private String id_global;
	@Column private String pathFromRoot;
	@Column private Integer rootFolder;

	// # auto-generated

	public String getId_global() {
		return id_global;
	}

	public void setId_global(String id_global) {
		this.id_global = id_global;
	}

	public Long getId_local() {
		return id_local;
	}

	public void setId_local(Long id_local) {
		this.id_local = id_local;
	}

	/**
	 * Note: Contains a trailing slash
	 */
	public String getPathFromRoot() {
		return pathFromRoot;
	}

	public void setPathFromRoot(String pathFromRoot) {
		this.pathFromRoot = pathFromRoot;
	}

	public Integer getRootFolder() {
		return rootFolder;
	}

	public void setRootFolder(Integer rootFolder) {
		this.rootFolder = rootFolder;
	}
}
