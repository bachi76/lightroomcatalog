package ch.insign.lightroom.model;

import ch.insign.lightroom.dao.LrImageDao;
import com.j256.ormlite.logger.LoggerFactory;
import com.j256.ormlite.table.DatabaseTable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Image cache entries in the lr preview db.
 */
@Entity(name = "ImageCacheEntry")
public class LrImageCacheEntry {

	@Id private Long imageId;
	@Column private String uuid;
	@Column private String digest;
	@Column private String orientation;

	// # auto-generated

	public String getDigest() {
		return digest;
	}

	public void setDigest(String digest) {
		this.digest = digest;
	}

	public Long getImageId() {
		return imageId;
	}

	public void setImageId(Long imageId) {
		this.imageId = imageId;
	}

	public String getOrientation() {
		return orientation;
	}

	public void setOrientation(String orientation) {
		this.orientation = orientation;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
}
