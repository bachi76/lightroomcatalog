package ch.insign.lightroom.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * References images in a collection.
 */
@Entity(name = "AgLibraryCollectionImage")
public class LrCollectionImage {

	@Id private Long id_local;
	@Column private Long collection;
	@Column private Long image;
	@Column private Integer positionInCollection;

	// # auto-generated

	public Long getCollection() {
		return collection;
	}

	public void setCollection(Long collection) {
		this.collection = collection;
	}

	public Long getId_local() {
		return id_local;
	}

	public void setId_local(Long id_local) {
		this.id_local = id_local;
	}

	public Long getImage() {
		return image;
	}

	public void setImage(Long image) {
		this.image = image;
	}

	public Integer getPositionInCollection() {
		return positionInCollection;
	}

	public void setPositionInCollection(Integer positionInCollection) {
		this.positionInCollection = positionInCollection;
	}
}
