package ch.insign.lightroom.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * References images in a collection.
 */
@Entity(name = "AgLibraryFolderStackImage")
public class LrFolderImage {

	@Id	private Long id_local;
	@Column private Long image;
	@Column private Integer position;
}
