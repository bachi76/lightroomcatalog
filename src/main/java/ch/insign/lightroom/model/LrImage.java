package ch.insign.lightroom.model;

import ch.insign.lightroom.dao.LrImageDao;
import com.j256.ormlite.table.DatabaseTable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Access lightroom images.
 *
 */
@Entity(name = "Adobe_images")
@DatabaseTable(daoClass = LrImageDao.class)
public class LrImage {
	@Id	private Long id_local;
	@Column private String id_global;
	@Column private String captureTime; // TODO: date format
	@Column private String fileFormat;
	@Column private Integer fileHeight;
	@Column private Integer fileWidth;
	@Column private String orientation;
	@Column private String positionInFolder;
	@Column private Long pyramidIDCache;
	@Column private Integer rating;
	@Column private Long rootFile;

	// # auto-generated

	public String getCaptureTime() {
		return captureTime;
	}

	public void setCaptureTime(String captureTime) {
		this.captureTime = captureTime;
	}

	public String getFileFormat() {
		return fileFormat;
	}

	public void setFileFormat(String fileFormat) {
		this.fileFormat = fileFormat;
	}

	public Integer getFileHeight() {
		return fileHeight;
	}

	public void setFileHeight(Integer fileHeight) {
		this.fileHeight = fileHeight;
	}

	public Integer getFileWidth() {
		return fileWidth;
	}

	public void setFileWidth(Integer fileWidth) {
		this.fileWidth = fileWidth;
	}

	public String getId_global() {
		return id_global;
	}

	public void setId_global(String id_global) {
		this.id_global = id_global;
	}

	public Long getId_local() {
		return id_local;
	}

	public void setId_local(Long id_local) {
		this.id_local = id_local;
	}

	public String getOrientation() {
		return orientation;
	}

	public void setOrientation(String orientation) {
		this.orientation = orientation;
	}

	public String getPositionInFolder() {
		return positionInFolder;
	}

	public void setPositionInFolder(String positionInFolder) {
		this.positionInFolder = positionInFolder;
	}

	public Long getPyramidIDCache() {
		return pyramidIDCache;
	}

	public void setPyramidIDCache(Long pyramidIDCache) {
		this.pyramidIDCache = pyramidIDCache;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public Long getRootFile() {
		return rootFile;
	}

	public void setRootFile(Long rootFile) {
		this.rootFile = rootFile;
	}
}
