package ch.insign.lightroom;

import ch.insign.lightroom.dao.LrPreviewDao;
import ch.insign.lightroom.model.*;
import ch.insign.lightroom.util.JpegExtractor;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.LruObjectCache;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;

import java.io.File;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Provides access to the stored previews and cached images.
 */
public class LrPreviewStore {

	private final LrCatalog catalog;
	private final ConnectionSource connectionSource;
	private final String lrdataFolderPath;
	private final String imageFolderPath;

	private Dao<LrImageCacheEntry, Long> imageCacheEntryDao;
	private LrPreviewDao previewDao;

	public LrPreviewStore(LrCatalog lrCatalog, String lrdataFolderPath, String imageFolderPath) throws SQLException {

		this.catalog = lrCatalog;
		this.lrdataFolderPath = lrdataFolderPath;
		this.imageFolderPath = imageFolderPath;

		String databaseUrl = "jdbc:sqlite:" + lrdataFolderPath + "/previews.db";

		// Load the driver
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		// create a connection source to our database
		connectionSource = new JdbcConnectionSource(databaseUrl);

		System.out.println("Preview db connection status: " + connectionSource.getReadOnlyConnection().isTableExists("ImageCacheEntry"));

	}

	public ConnectionSource getConnectionSource() {
		return connectionSource;
	}

	/**
	 * Supposedly image cache entries, but better use previews().
	 * @throws SQLException
	 */
	@Deprecated
	public Dao<LrImageCacheEntry, Long> cacheEntries() throws SQLException {
		if (imageCacheEntryDao ==  null) {
			imageCacheEntryDao = DaoManager.createDao(connectionSource, LrImageCacheEntry.class);
			imageCacheEntryDao.setObjectCache(new LruObjectCache(LrCatalog.LRU_CACHE_SIZE));
		}
		return imageCacheEntryDao;
	}

	/**
	 * Access the different preview sizes of an image.
	 * @throws SQLException
	 */
	public LrPreviewDao previews() throws SQLException {
		if (previewDao ==  null) previewDao = DaoManager.createDao(connectionSource, LrPreview.class);
		return previewDao;
	}

	/**
	 * Extract the preview jps from a given lr preview file.
	 * The naming will match the original file's, the extracted jpgs will have an index number matching LrPreview's level.
	 *
	 * @param file the lr preview file to extract jpgs from
	 * @param relativePath output path relative to the image home folder.
	 * @return true if the file was found (not necessarily meaning it contained jpgs)
	 * @throws SQLException
	 */
	public boolean extract(LrFile file, String relativePath) throws SQLException {


		List<LrImageCacheEntry> cache = cacheEntries().queryForEq("uuid", file.getId_global());
		if (cache.size() == 0) {
			System.out.println("No cache entry found for uuid " + file.getId_global());
			return false;
		}
		LrImageCacheEntry cacheEntry = cache.get(0); // FIXME: Check how to handle multiple entries.

		String prevPath = lrdataFolderPath
			+ "/" + file.getId_global().substring(0,1)
			+ "/" + file.getId_global().substring(0,4)
			+ "/" + cacheEntry.getUuid() + "-" + cacheEntry.getDigest() + ".lrprev";

		// Find lrprev file
		File prevFile = new File(prevPath);

		if (!prevFile.isFile()) {
			System.out.println("Preview file not found: " + prevPath);
			return false;
		}

		String outputFolder = imageFolderPath + "/" + relativePath;

		// Extract now
		// Different image sizes are numbered, e.g _MBD7897-1.jpg, _MBD7897-2.jpg etc
		// The index number (hopefully) equals the level of LrPreview
		JpegExtractor extractor = new JpegExtractor();
		extractor.extract(prevPath, outputFolder, file.getBaseName() + "-", 1);

		return true;
	}

	/**
	 * Get the preview file if available.
	 */
	public Optional<File> getPreview(LrFile file, LrPreview preview) {

		try {
			LrFolder folder = catalog.folders().queryForId(file.getFolder());
			String path = imageFolderPath
				+ "/" + folder.getPathFromRoot()
				+ /*"/" +*/ file.getBaseName() + "-" + preview.getLevel() + ".jpg";

			// Check if the preview exists
			File prevFile = new File(path);
			if (prevFile.isFile()) {
				System.out.println("Returning preview: " + prevFile.getAbsolutePath());
				return Optional.of(prevFile);

			} else {
				// Preview file not found - trying to extract it
				System.out.println("Preview not found - trying to extract: " + path);
				extract(file, folder.getPathFromRoot());

				// Check again if the preview exists now
				File prevFile2 = new File(path);
				if (prevFile2.isFile()) {
					System.out.println("Returning preview: " + prevFile2.getAbsolutePath());
					return Optional.of(prevFile2);

				} else {

					// Definitively does not exist.
					System.out.println("Preview defintively not there: " + prevFile2.getAbsolutePath());
					return Optional.empty();
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return Optional.empty();
		}
	}

	/**
	 * Get the file for the preview of the given level or smaller.
	 * @param level max. level (1-n, higher level = higher resolution). If not available, the highest available will be used
	 */
	public Optional<File> getPreview(LrFile file, int level) {

		try {
			List<LrPreview> prevs = previews().forFile(file);

			// Reverse order so highest levels come first
			// Take the first with the desired or smaller level
			Collections.reverse(prevs);
			for (LrPreview prev : prevs) {
				if (prev.getLevel() <= level) {
					return getPreview(file, prev);
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return Optional.empty();
	}

	/**
	 * Get the root folder of the extracted images.
	 */
	public String getImageFolderPath() {
		return imageFolderPath;
	}

}
